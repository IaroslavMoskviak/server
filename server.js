const http = require("http");
const port = 8080;
const fs = require("fs");

const app = http.createServer((req, res) => {
  res.writeHead(200);
  fs.readFile("./public/hello.txt", (err, data) => {
    if (err) {
      res.writeHead(404);
      res.write("ERROR: file not found");
    } else {
      res.write(data.toString());
    }
    res.end();
  });
});

app.listen(port, (err) => {
  if (err) {
    return console.log("something bad happened", err);
  }
  console.log(`server is listening on ${port}`);
});
